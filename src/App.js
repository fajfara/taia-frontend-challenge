// Libs
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import { Provider } from 'react-redux';

// Components
import Header from 'components/layout/Header';

// Pages
import Home from 'pages/Home';
import Recipe from 'pages/Recipe';
import Page404 from 'pages/404';
import About from 'pages/About';

// Redux
import store from 'store/store';

const App = () => {
  return (
    <Provider store={store}>
      <Router>
        <Header />
        <Switch>
          <Route exact path="/">
            <Home />
          </Route>
          <Route path="/about">
            <About />
          </Route>
          <Route path="/recipe/:id">
            <Recipe />
          </Route>
          <Route>
            <Page404 />
          </Route>
        </Switch>
      </Router>
    </Provider>
  );
};

export default App;
