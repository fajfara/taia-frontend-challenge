import axios from 'axios';
/**
 * Create axios instance and add default url, headers and params
 */

// Default headers
const headers = {
  Accept: 'application/json',
  'Content-Type': 'application/json',
};

// Default url
const baseURL = process.env.REACT_APP_LIVE_API
  ? process.env.REACT_APP_SPOONACULAR_API_URL
  : 'http://localhost:3000';

// Create axios instance
const api = axios.create({
  baseURL,
  headers,
  params: {
    apiKey: process.env.REACT_APP_SPOONACULAR_API_KEY,
  },
});

export default api;
