import Container from 'components/layout/Container';

/**
 * Renders a banner
 *
 * @param {string} width
 *  What width should the banner be
 * @param {string} bgImage
 *  Banner background image
 */
const Banner = ({ width = 'contained', bgImage, ...rest }) => {
  const props = { ...rest };

  // Build classname
  if (!props.className) props.className = '';

  if (width === 'contained') props.className += ' container mx-auto';

  props.className += ' relative';

  return (
    <section {...props}>
      <div className="z-10 relative">
        {/* Do a check if width is full-width, if so wrap children in container */}
        {width === 'full-width' ? <Container>{rest.children}</Container> : rest.children}
      </div>
      {/* Display bg image if its set */}
      {bgImage && (
        <div className="absolute top-0 left-0 h-full w-full">
          <img className=" h-full w-full z-0 object-cover" src={bgImage} alt="" />
          <div className="absolute top-0 left-0 w-full h-full bg-black opacity-50"></div>
        </div>
      )}
    </section>
  );
};

export default Banner;
