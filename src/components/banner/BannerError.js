// Components
import Heading from 'components/typography/Heading';
import Section from 'components/layout/Section';

/**
 * Renders an error banner
 *
 * @param {string} title
 * @param {string} msg
 */
const BannerError = ({ title = 'Error!', msg }) => {
  return (
    <Section topSpacing={false} className="bg-red-600 p-8">
      <Heading className="mb-4 text-white">{title}</Heading>
      {msg && <p className="text-white">{msg}</p>}
    </Section>
  );
};

export default BannerError;
