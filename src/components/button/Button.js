/**
 * Simple button component
 *
 * @param {React.ReactNode} children
 * @param {string} type
 * @param {string} size
 * @param {string} className
 */
const Button = ({ children, type = 'primary', size = 'md', className }) => {
  const btnTypes = {
    primary: 'bg-blue-500 text-white',
  };

  const btnSizes = {
    md: 'py-2 px-4',
  };

  const props = {
    className: `${btnTypes[type]} ${btnSizes[size]} ${className}`,
  };

  return <button {...props}>{children}</button>;
};

export default Button;
