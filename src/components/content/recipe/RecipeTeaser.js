import PropTypes from 'prop-types';

// Libs
import { Link } from 'react-router-dom';

// Components
import Heading from 'components/typography/Heading';

/**
 * Renders recipe teaser card
 *
 * @param {string} title
 *  Recipe title
 * @param {string} image
 *  Recipe image src url
 * @param {string} id
 *  Recipe id, used as link for router
 * @param {number} titleLevel
 *  Title level
 * @param {string} className
 */
const RecipeTeaser = ({ title, image, id, titleLevel = 3, className }) => {
  const props = { className };

  if (!props.className) props.className = '';

  props.className += ' w-full h-full flex flex-row relative bg-gray-50 p-2';

  return (
    <div {...props}>
      {title && (
        <Heading
          styledAs={4}
          level={titleLevel}
          className={`py-2 mr-2 ${image ? 'w-1/2' : 'w-full'}`}
        >
          {title}
        </Heading>
      )}
      {image && (
        <img
          className="w-1/2 h-auto object-cover"
          src={image}
          alt={`Quick preview of recipe titled ${title}`}
        />
      )}
      {id && (
        <Link
          to={`/recipe/${id}`}
          title="Go to single recipe page"
          className="absolute top-0 left-0 w-full h-full"
        >
          <span style={{ fontSize: 0 }}>Go to single recipe page</span>
        </Link>
      )}
    </div>
  );
};

RecipeTeaser.propTypes = {
  title: PropTypes.string,
  image: PropTypes.string,
};

export default RecipeTeaser;
