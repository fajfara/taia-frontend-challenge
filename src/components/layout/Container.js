/**
 * Simple container component
 *
 * @param {React.ReactNode} children
 * @param {string} className
 */
const Container = ({ children, className = '' }) => {
  return <div className={`container mx-auto px-8 sm:px-8 ${className}`}>{children}</div>;
};

export default Container;
