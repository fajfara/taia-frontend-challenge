// Libs
import { Link } from 'react-router-dom';

// Components
import Heading from 'components/typography/Heading';
import BasicNavigation from 'components/navigation/BasicNavigation';
import Search from 'components/search/Search';
import Container from 'components/layout/Container';

/**
 * Render header
 */
const Header = () => {
  const links = [
    {
      to: '/',
      title: 'Home',
    },
    {
      to: '/about',
      title: 'About',
    },
  ];

  return (
    <header className="sticky top-0 bg-white z-20">
      <div className="border-b border-gray-500 border-solid">
        <Container className="flex justify-between py-7">
          <Link to="/">
            <Heading displayAs="p" styledAs={1}>
              Recipes
            </Heading>
          </Link>

          <BasicNavigation links={links} />
        </Container>
      </div>

      <div className="border-b border-gray-500 border-solid">
        <Container>
          <Search />
        </Container>
      </div>
    </header>
  );
};

export default Header;
