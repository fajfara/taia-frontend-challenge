import Container from 'components/layout/Container';
import React from 'react';

/**
 * Renders a section with default spacing top and bottom
 *
 * @param {boolean} container
 *  If display in container, by default true
 * @param {boolean} topSpacing
 *  If top spacing is added, by default true
 * @param {React.ReactNode} children
 */
const Section = ({ container = true, topSpacing = true, children, ...rest }) => {
  const props = {
    ...rest,
  };

  // Build css classes
  if (!props.className) props.className = '';

  if (topSpacing) props.className += ' pt-16';

  // Create wrapper, conditionally wrap in container if set to true
  let Wrapper = React.Fragment;

  if (container) {
    Wrapper = Container;
  }

  return (
    <section {...props}>
      <Wrapper>{children}</Wrapper>
    </section>
  );
};

export default Section;
