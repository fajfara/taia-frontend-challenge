// Hooks
import useFetch from 'hooks/useFetch';

// Libs
import ClockLoader from 'react-spinners/ClockLoader';

// Components
import Heading from 'components/typography/Heading';
import RecipeTeasersList from './RecipeTeasersList';
import Section from 'components/layout/Section';
import BannerError from 'components/banner/BannerError';
import Container from 'components/layout/Container';

/**
 * Render random recipes
 */
const RandomRecipes = () => {
  // Variables
  const pathToFetch = process.env.REACT_APP_LIVE_API
    ? 'recipes/random'
    : 'dummy-data/random-recipes.json';

  // State
  const { data, fetching, error } = useFetch(pathToFetch, { number: 6 });

  if (error.show) {
    return (
      <Container className="px-8 sm:px-32">
        <Section>
          <BannerError msg={error.msg} />
        </Section>
      </Container>
    );
  }

  if (fetching || !data) {
    return (
      <Container className="px-8 sm:px-32">
        <Section className="flex flex-col items-center" container={false}>
          <ClockLoader className="block mb-8" size={80} />
          <Heading className="mt-8">Loading data...</Heading>
        </Section>
      </Container>
    );
  }

  return (
    <>
      <Heading className="mb-8">Checkout some of our recipes</Heading>
      <RecipeTeasersList items={data.recipes} />
    </>
  );
};

export default RandomRecipes;
