// Components
import Heading from 'components/typography/Heading';

/**
 * Render steps
 *
 * @param {array} steps
 *  An array of steps { step: [], number: number, ingredients; [], equipment: [] }
 */
const RecipeSteps = ({ steps }) => {
  return (
    <ul>
      {steps.map(({ step: instructions, number, ingredients, equipment }) => (
        <li key={number} className="mb-8 bg-gray-50 shadow p-8">
          <Heading styledAs={3} className="mb-4">
            Step number {number}
          </Heading>
          <p className="py-2 border-t border-b border-gray-500 border-solid mb-4">{instructions}</p>
          {ingredients.length > 0 && (
            <section className="mb-4">
              <Heading level={3} displayAs="p" styledAs={4} className="mb-2">
                Ingredients
              </Heading>
              <ul className="pl-6">
                {ingredients.map(({ id, name, image }) => (
                  <li key={`${id}__${name}`} className="list-disc">
                    {name}
                  </li>
                ))}
              </ul>
            </section>
          )}
          {equipment.length > 0 && (
            <section>
              <Heading level={3} displayAs="p" styledAs={4} className="mb-2">
                Equipment
              </Heading>
              <ul className="pl-6">
                {equipment.map(({ id, name, image }) => (
                  <li className="list-disc" key={`${id}__${name}`}>
                    {name}
                  </li>
                ))}
              </ul>
            </section>
          )}
        </li>
      ))}
    </ul>
  );
};

export default RecipeSteps;
