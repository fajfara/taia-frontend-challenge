// Components
import RecipeTeaser from 'components/content/recipe/RecipeTeaser';

/**
 * Renders a list of recipes
 *
 * @param {array} items
 *  Array of recipes { title: string, image: string }
 */
const RecipeTeasersList = ({ items, titleLevel = 3, className, ...rest }) => {
  if (!items) return <h2>No items provided</h2>;

  return (
    <ul className={`grid sm:grid-cols-2 lg:grid-cols-3 gap-4 w-full h-full ${className}`} {...rest}>
      {items.map((recipe, idx) => (
        <li key={idx}>
          <RecipeTeaser titleLevel={titleLevel} {...recipe} />
        </li>
      ))}
    </ul>
  );
};

export default RecipeTeasersList;
