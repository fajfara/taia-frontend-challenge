import { Link } from 'react-router-dom';

/**
 * Component that renders basic navigation from passed links array
 *
 * @param {array} links { to: string, title: string }[]
 *  An array of links
 */
const BasicNavigation = ({ links }) => {
  return (
    <nav>
      <ul className="flex h-full">
        {links.map(({ to, title }, index) => (
          <li
            key={index}
            className={`flex items-center ${links.length !== index + 1 ? 'mr-4' : ''}`}
          >
            <Link {...{ to, title }}>{title}</Link>
          </li>
        ))}
      </ul>
    </nav>
  );
};

export default BasicNavigation;
