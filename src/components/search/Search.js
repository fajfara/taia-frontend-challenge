import { useEffect, useState, useRef } from 'react';

// Axios instance
import api from 'api/api';

// Libs
import { useDebounce } from 'use-debounce';

// Components
import SearchResults from 'components/search/SearchResults';
import Button from 'components/button/Button';
import BannerError from 'components/banner/BannerError';

// Hooks
import useOnClickOutside from 'hooks/useClickOutside';

/**
 * Render search bar
 */
const Search = () => {
  // Variables
  const pathToFetch = process.env.REACT_APP_LIVE_API
    ? `recipes/complexSearch`
    : 'dummy-data/search-results-6.json';

  // State
  const [results, setResults] = useState([]);
  const [inputValue, setInputValue] = useState('');
  const [mounted, setMounted] = useState(false);
  const [resultsShowState, setResultsShowState] = useState('hide');
  const [errorMsg, setErrorMsg] = useState(null);

  // Ref
  const ref = useRef();

  // Hooks
  const [searchQuery] = useDebounce(inputValue, 300);

  useOnClickOutside(ref, () => setResultsShowState('hide'));

  // Methods

  // Effects
  /**
   * Mounted effect
   */
  useEffect(() => {
    setMounted(true);
  }, []);

  /**
   * Search query change effect, debounced
   */
  useEffect(() => {
    if (!mounted) return;

    const fetchResults = async () => {
      if (searchQuery === '') {
        setErrorMsg(null);
        return;
      }

      try {
        const response = await api.get(pathToFetch, {
          params: {
            query: searchQuery,
            number: 6,
          },
        });

        if (response.data.results) {
          setResults(response.data.results);
          setResultsShowState('show');
        }
      } catch (e) {
        setErrorMsg(e.response.data.message ? e.response.data.message : '');
      }
    };

    fetchResults();
  }, [searchQuery, pathToFetch, mounted]);

  return (
    <div className="relative py-4" ref={ref}>
      <form className="flex w-full">
        <input
          className="w-1/2 p-2 border border-gray-500 border-solid flex-grow sm:flex-grow-0"
          type="text"
          name="search"
          onChange={(e) => setInputValue(e.target.value)}
          onFocus={() => setResultsShowState('show')}
          value={inputValue}
          placeholder="Search for a recipe..."
        />
        <Button className="ml-4">Search</Button>
      </form>
      <div onClick={() => setResultsShowState('hide')}>
        {!errorMsg && (
          <SearchResults results={results} showState={resultsShowState} searchQuery={searchQuery} />
        )}
        {errorMsg && <BannerError msg={errorMsg} />}
      </div>
    </div>
  );
};

export default Search;
