import { memo, useState, useEffect } from 'react';

// Components
import RecipeTeasersList from 'components/lists/RecipeTeasersList';

/**
 * Render search results
 */
const SearchResults = ({ results, showState, searchQuery, ...rest }) => {
  // State
  const [resultComponent, setResultComponent] = useState(<h2>Type in the query</h2>);

  // Make props
  const props = {
    ...rest,
  };

  if (!props.className) props.className = '';

  props.className +=
    ' opacity-0 absolute bg-white w-full pointer-events-none transition-opacity transform-gpu duration-300 ease-in-out p-8 top-full border border-gray-500 border-solid overflow-hidden';

  if (showState === 'show') props.className += ' opacity-100 pointer-events-auto';

  // Effects
  /**
   * Runs when searchQuery changes or results change,
   * set a component to display
   */
  useEffect(() => {
    if (searchQuery === '') {
      setResultComponent(<h2>Hungry?</h2>);
      return;
    }
    if (results.length > 0) {
      setResultComponent(<RecipeTeasersList items={results} />);
      return;
    }
    if (results.length === 0) {
      setResultComponent(<h2>No results found</h2>);
      return;
    }
  }, [searchQuery, results]);

  return <div {...props}>{resultComponent}</div>;
};

export default memo(SearchResults);
