import React from 'react';
import PropTypes from 'prop-types';

// Default styling for headings
const headingStyles = {
  1: 'text-5xl',
  2: 'text-3xl',
  3: 'text-xl',
  4: 'text-lg',
};

/**
 * Component used to render a heading or an element styled as a heading
 *
 * @param {number} level
 *  Heading level, used if displayAs is set to the heading tag - h
 * @param {number} styledAs
 *  Styled as what heading level
 * @param {string} displayAs
 *  String that sets the tag to be rendered
 *
 * Use ...rest to spread any other props to the rendered tag
 */
const Heading = ({ level = 2, styledAs = 2, displayAs = 'h', ...rest }) => {
  // generate tag to be used in render
  const Tag = displayAs === 'h' ? displayAs + level : displayAs;

  const headingProps = {
    ...rest,
  };

  // Generate className
  const cssClasses = ['font-bold font-playfair-display'];

  // Check if styledAs is set
  cssClasses.push(headingStyles[styledAs ? styledAs : level]);

  // Check if className is set already, if so join with new classes
  if (rest.className) cssClasses.push(rest.className);
  headingProps.className = cssClasses.join(' ');

  return <Tag {...headingProps}>{rest.children}</Tag>;
};

// Prop types
Heading.propTypes = {
  level: PropTypes.number,
  styledAs: PropTypes.number,
  displayAs: PropTypes.string,
};

export default React.memo(Heading);
