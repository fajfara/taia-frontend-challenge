import { useState, useEffect } from 'react';

import api from 'api/api';

/**
 * Simple fetch hook
 *
 * @param {string} path
 * @param {object} params
 */
const useFetch = (path, params = {}) => {
  const [data, setData] = useState(null);
  const [fetching, setFetching] = useState(false);
  const [error, setError] = useState({ show: false, msg: null });

  useEffect(() => {
    const fetchData = async () => {
      try {
        setFetching(true);

        const response = await api.get(path, { params });

        if (response.data) {
          setData(response.data);
          setFetching(false);
        } else {
          setError({ show: true, msg: 'No data found' });
          setFetching(false);
        }
      } catch (e) {
        console.error(e);
        setFetching(false);
        setError({ show: true, msg: e.response.data.message ? e.response.data.message : '' });
      }
    };

    fetchData();
  }, [path]);

  return { data, fetching, error };
};

export default useFetch;
