import { useEffect, useRef } from 'react';

/**
 * Mounted hook
 */
const useIsMounted = () => {
  const mounted = useRef(false);

  useEffect(() => {
    mounted.current = true;
    return () => {
      mounted.current = false;
    };
  }, []);

  return mounted;
};

export default useIsMounted;
