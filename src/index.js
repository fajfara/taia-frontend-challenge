import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';

import 'styles/tailwind.css';
import 'styles/aspect-ratio.css';

ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById('root'),
);
