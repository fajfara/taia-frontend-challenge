// Components
import Heading from "components/typography/Heading";
import Section from "components/layout/Section";

// Image
import image404 from "assets/images/404.jpg";

const Page404 = () => {
    return (
        <Section className="px-32 pb-16 flex flex-col">
            <Heading className="mb-8 text-center" level={1} styledAs={1}>Page not found</Heading>
            <img className="w-1/2 mx-auto" src={image404} alt="" />
        </Section>
    )
}

export default Page404
