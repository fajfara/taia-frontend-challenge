// Components
import Heading from 'components/typography/Heading';
import Section from 'components/layout/Section';

const About = () => {
  return (
    <Section className="px-32 pb-16 flex flex-col text-center">
      <Heading className="mb-8 text-center" level={1} styledAs={1}>
        About
      </Heading>
      <p>Made by Anže Fajfar</p>
    </Section>
  );
};

export default About;
