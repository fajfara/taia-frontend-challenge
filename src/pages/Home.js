// Components
import Heading from 'components/typography/Heading';
import RecipeTeasersList from 'components/lists/RecipeTeasersList';
import Section from 'components/layout/Section';
import Banner from 'components/banner/Banner';

// Images
import bannerBgImage from 'assets/images/cooking-background.jpg';
import RandomRecipes from 'components/lists/RandomRecipes';

// Redux
import { useSelector } from 'react-redux';

/**
 * Homepage component
 */
const Home = () => {
  const { recipes: lastViewedRecipes } = useSelector((state) => state.lastViewedRecipes);

  return (
    <div>
      <Banner className="bg-blue-100" width="full-width" bgImage={bannerBgImage}>
        <Heading level={1} styledAs={1} className="py-32 text-white">
          Welcome to the recipe app
        </Heading>
      </Banner>
      <Section>
        <RandomRecipes />
      </Section>
      <Section>
        <Heading className="mb-8">Recently viewed recipes</Heading>
        <RecipeTeasersList items={lastViewedRecipes} />
      </Section>
    </div>
  );
};

export default Home;
