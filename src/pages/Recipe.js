// Libs
import { useParams } from 'react-router-dom';
import ClockLoader from 'react-spinners/ClockLoader';
import { useEffect } from 'react';

// Components
import Heading from 'components/typography/Heading';
import RecipeSteps from 'components/lists/RecipeSteps';
import Section from 'components/layout/Section';
import RecipeTeasersList from 'components/lists/RecipeTeasersList';
import BannerError from 'components/banner/BannerError';

// Hooks
import useFetch from 'hooks/useFetch';

// Redux
import { addRecipeToLastViewed } from 'store/reducers/LastViewedRecipeReducer';
import { useDispatch } from 'react-redux';

const Recipe = () => {
  // Redux
  const dispatch = useDispatch();

  // React router - params
  const { id } = useParams();

  // Variables
  // Path to use for fetching data
  const pathRecipeData = process.env.REACT_APP_LIVE_API
    ? `recipes/${id}/information`
    : 'dummy-data/single-recipe.json';

  const pathSimilarRecipes = process.env.REACT_APP_LIVE_API
    ? `recipes/${id}/similar`
    : 'dummy-data/search-results-6.json';

  // State
  const { data: recipeData, fetching, error } = useFetch(pathRecipeData, { number: 6 });
  const {
    data: similarRecipes,
    fetching: fetchingSimilarRecipes,
    error: errorSimilarRecipes,
  } = useFetch(pathSimilarRecipes, { number: 6 });

  // Effects
  useEffect(() => {
    if (recipeData) {
      dispatch(addRecipeToLastViewed(recipeData));
    }
  }, [recipeData]);

  if (error.show || errorSimilarRecipes.show) {
    return (
      <div className="container mx-auto px-8 sm:px-32">
        <BannerError msg={error.msg || errorSimilarRecipes.msg} />
      </div>
    );
  }

  if (fetching || fetchingSimilarRecipes || !recipeData || !similarRecipes) {
    return (
      <div className="container mx-auto px-8 sm:px-32">
        <Section className="flex flex-col items-center" container={false}>
          <ClockLoader className="block mb-8" size={70} />
          <Heading className="mt-8">Loading data...</Heading>
        </Section>
      </div>
    );
  }

  return (
    <div>
      <article>
        <div className="container mx-auto mt-8 px-8 sm:px-32 pb-16">
          <header>
            <Heading level={1}>{recipeData.title}</Heading>
            <div className="mt-8">
              <img
                className="h-32 w-32 float-left mr-4"
                src={recipeData.image}
                alt="Preview of the finished dish"
              />
              <p dangerouslySetInnerHTML={{ __html: recipeData.summary }}></p>
            </div>
          </header>

          {recipeData.extendedIngredients && (
            <Section topSpacing={false} className="mt-8" container={false}>
              <Heading className="mb-4" styledAs={3}>
                Ingredients
              </Heading>
              <ul className="pl-6">
                {recipeData.extendedIngredients.map(({ id, name, measures: { metric } }, index) => (
                  <li key={`${id}__${name}__ingredient__${index}`} className="list-disc">
                    {name} x {metric.amount} {metric.unitShort}
                  </li>
                ))}
              </ul>
            </Section>
          )}

          {recipeData.instructions && (
            <Section container={false}>
              <Heading className="mb-4" styledAs={3}>
                Quick instructions
              </Heading>
              <p dangerouslySetInnerHTML={{ __html: recipeData.instructions }}></p>
            </Section>
          )}

          {recipeData.analyzedInstructions.length > 0 && (
            <Section container={false}>
              <Heading styledAs={3} className="mb-4">
                Step by step
              </Heading>
              <RecipeSteps steps={recipeData.analyzedInstructions[0].steps} />
            </Section>
          )}

          {similarRecipes.length > 0 && (
            <Section container={false}>
              <Heading styledAs={3} className="mb-4">
                Similar recipes
              </Heading>
              <RecipeTeasersList items={similarRecipes} />
            </Section>
          )}
        </div>
      </article>
    </div>
  );
};

export default Recipe;
