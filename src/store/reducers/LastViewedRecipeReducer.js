const ADD_RECIPE = 'add_recipe';

export const addRecipeToLastViewed = (data) => ({
  type: ADD_RECIPE,
  payload: data,
});

const initialState = {
  recipes: [],
};

const LastViewedRecipesReducer = (state = initialState, action) => {
  switch (action.type) {
    case ADD_RECIPE:
      const currRecipes = [...state.recipes];

      console.log(currRecipes.find((recipe) => recipe.id === action.payload.id));

      // Check if recipe already in history
      if (currRecipes.find((recipe) => recipe.id === action.payload.id)) {
        return { recipes: [...currRecipes] };
      }

      // Check length of current recipes
      if (currRecipes.length === 3) {
        currRecipes.shift();
      }

      currRecipes.push(action.payload);

      return { recipes: [...currRecipes] };
    default:
      return state;
  }
};

export default LastViewedRecipesReducer;
