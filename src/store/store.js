import { combineReducers, createStore } from 'redux';

import LastViewedRecipesReducer from './reducers/LastViewedRecipeReducer';

const reducers = combineReducers({
  lastViewedRecipes: LastViewedRecipesReducer,
});

const store = createStore(reducers);

export default store;
